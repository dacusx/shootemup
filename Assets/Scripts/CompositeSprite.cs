using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

public class CompositeSprite : MonoBehaviour
{
    [SerializeField] private Vector2Int size;
    [SerializeField] private Sprite[] sprites;
    [SerializeField] private SpriteRenderer spriteRenderer;

    private void Start()
    {
        if (sprites.Length != size.x * size.y)
            throw new ArgumentException($"Wrong number of sprites {sprites.Length}, should be {size.x * size.y}.");

        for (int i = 0; i < sprites.Length; i++)
        {
            Sprite sprite = sprites[i];

            NativeArray<Color32> data = sprite.texture.GetRawTextureData<Color32>();
        }

        // spriteRenderer.sprite.texture
    }
}
