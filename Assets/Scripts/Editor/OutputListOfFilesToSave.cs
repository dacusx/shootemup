﻿using System.Text;
using UnityEngine;

public class OutputListOfFilesToSave : UnityEditor.AssetModificationProcessor
{
    //Will be invoked once for each call to CreateAsset()
    //and once for calling AssetDatabase.SaveAssets()
    private static string[] OnWillSaveAssets(string[] paths)
    {
        Debug.Log("OnWillSaveAssets invoked");

        if (paths.Length <= 0)
        {
            Debug.Log("No assets to be saved.");

            return paths;
        }

        StringBuilder assetsToBeSaved = new StringBuilder();
        assetsToBeSaved.AppendLine();

        foreach (string path in paths)
        {
            assetsToBeSaved.Append(path);
            assetsToBeSaved.AppendLine();
        }

        Debug.Log("Assets to be saved:" + assetsToBeSaved.ToString());

        return paths;
    }
}