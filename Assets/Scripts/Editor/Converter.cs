using System.Collections.Generic;
using System.IO;
using System.Linq;
using Unity.Collections;
using UnityEditor;
using UnityEngine;
using File = UnityEngine.Windows.File;

public static class Converter
{
    private const string sourceSubPath = "Gfx/Remastered Tyrian Graphics";
    private const string destinationSubPath = "Gfx/Converted";

    [MenuItem("Tools/Convert GFX")]
    private static void ConvertGfx()
    {
        FileInfo[] files = Directory.GetFiles(Path.Combine("Assets", sourceSubPath)).Where(s => s.EndsWith(".png"))
            .Select(s => new FileInfo(s)).ToArray();

        foreach (FileInfo file in files)
        {
            string filename = file.Name;

            string sourcePath = Path.Combine("Assets", sourceSubPath, filename);

            Sprite sprite = AssetDatabase.LoadAssetAtPath<Sprite>(sourcePath);

            Texture2D texture = CopyTextureWithTransparency(sprite.texture);

            string destinationPath = Path.Combine(Application.dataPath, destinationSubPath, filename);

            Directory.CreateDirectory(Path.Combine(Application.dataPath, destinationSubPath));

            SaveToPng(texture, destinationPath);

            AssetDatabase.Refresh();

            ChangeImportSettings(Path.Combine("Assets", destinationSubPath, filename), texture.width, texture.height);

            // break;
        }

        AssetDatabase.SaveAssets();
    }

    private static void SaveToPng(Texture2D texture, string path)
    {
        File.WriteAllBytes(path, texture.EncodeToPNG());
    }

    private static Texture2D CopyTextureWithTransparency(Texture2D texture)
    {
        Color32 transparentColor = texture.GetPixel(0, 0);

        Texture2D result = new Texture2D(texture.width, texture.height, TextureFormat.RGBA32, false);

        NativeArray<Color32> data = texture.GetRawTextureData<Color32>();

        for (int i = 0; i < data.Length; i++)
            if (data[i].Equals(transparentColor))
                data[i] = Color.clear;

        result.LoadRawTextureData(data);

        result.Apply();

        return result;
    }

    private static void ChangeImportSettings(string path, int width, int height)
    {
        int sheetWidth = width / Settings.SpriteWidth;
        int sheetHeight = height / Settings.SpriteHeight;

        string name = Path.GetFileNameWithoutExtension(path);

        TextureImporter textureImporter = (TextureImporter)AssetImporter.GetAtPath(path);

        // Change texture settings
        textureImporter.filterMode = FilterMode.Point;
        textureImporter.spriteImportMode = SpriteImportMode.Multiple;
        textureImporter.isReadable = true;

        List<SpriteMetaData> sprites = new List<SpriteMetaData>();

        for (int y = 0; y < sheetHeight; y++)
            for (int x = 0; x < sheetWidth; x++)
                sprites.Add(new SpriteMetaData
                {
                    name = name + $"_{x}_{sheetHeight - y - 1}",
                    rect = new Rect(x * Settings.SpriteWidth, y * Settings.SpriteHeight, Settings.SpriteWidth,
                        Settings.SpriteHeight)
                });

        textureImporter.spritesheet = sprites.ToArray();
        //

        textureImporter.SaveAndReimport();
    }
}
